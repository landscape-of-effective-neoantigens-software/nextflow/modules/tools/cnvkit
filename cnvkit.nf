process cnvkit_batch {

  tag "${dataset}/${pat_name}/${norm_run}_${tumor_run}"
  label 'cnvkit'
  label 'cnvkit_batch'
  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${norm_run}_${tumor_run}/cnvkit_batch"

  input:
    tuple val(pat_name), val(dataset), val(norm_run), path(norm_bam), path(norm_bai), val(tumor_run), path(tumor_bam), path(tumor_bai)
    path targets
    path fa
    val parstr
  output:
    tuple val(pat_name), val(dataset), val(norm_run), val(tumor_run), path("*cnvkit.outputs"), emit: cnvkit_outputs
  script:
  """
  cnvkit.py batch *-ad*bam --normal *-nd*bam \
      --targets ${targets} \
      --fasta ${fa} \
      --output-reference joint_normals.cnn \
      --output-dir ${dataset}-${pat_name}-${norm_run}_${tumor_run}.cnvkit.outputs \
      ${parstr}
  """
}
